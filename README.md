
* Clone this repo to your home directory on nyx.hpc.kyb.local

* sbatch. Submit the batch job to the cluster. Do:
$ sbatch nyx-sing-slurm.job.sh

* sbatch will tell you the job id.

* With the job id, you can check status using squeue. Do:
$ squeue

* When your job runs, it will be allocated a node. sinfo tells the status of nodes. Do:
$ sinfo

* To watch the output of your job, you can tail the output files. Do:
$ tail -f tjob.out.<job-id-from-sbatch>

* Keep an eye on tjob.err.<job-id-from-sbatch> for stderr messages.

* You can see the status of your jobs with squeue. Do:
$ squeue -u ${USER}

 




Command line just with singularity:

module load singularity
singularity run /ptmp/containers/<PICK A SINGULARITY CONTAINER> -nodesktop -nojvm -r "run('/ptmp/${USER}/workflow-slurm-matlab/cpu_bench.m'); exit;"
