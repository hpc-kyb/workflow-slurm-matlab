%--  parallel_mywave --%
maxNumCompThreads(64)
% A parfor loop will use parallel workers if available.
parfor i = 1:10  
    disp(['Iter is: ', num2str(i)])
end
for j = 1:10
    parfor i = 1:100000000
        A(i) = sin((i+j)*2*pi/2500000);
    end
end