
maxNumCompThreads(64)
%system('env')
%fprintf('pwd; %s', @pwd )
% Pick some numbers that will give time to look at the job running
% From the command line, use: 
% sinfo
% sbatch
% tail -f tjob.out.<jobid>
for N=[1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 4096, 4096, 4096] % 8192] %, 16384, 32768]

    system('/usr/bin/hostid');
    
    N_mult = 32768 / N;

    fprintf('Iter N = %d: ', N);

    rng(1);
    A = rand(N);
    B = rand(N);
    A_pd = A*A';

    tic;
    svd(A);
    t_svd = toc;
    fprintf('SVD ');

    tic;
    chol(A_pd);
    t_chol = toc;
    fprintf('Chol ');

    tic;
    qr(A);
    t_qr = toc;
    fprintf('QR ');

    tic;
    for k=1:N_mult
        A*B;
    end
    t_mult = toc;
    fprintf('%d mult ', N_mult);

    tic;
    inv(A);
    t_inv = toc;
    fprintf('Inv ');

    tic;
    pinv(A);
    t_pinv = toc;
    fprintf('Pinv\n\n');

    fprintf('TIME IN SECONDS (SIZE: %d):\n', N);
    fprintf('maxNumCompThreads %d\n', maxNumCompThreads );
    fprintf('SVD: %f\n', t_svd);
    fprintf('Cholesky: %f\n', t_chol);
    fprintf('QR: %f\n', t_qr);
    fprintf('%d matrix products: %f\n', N_mult, t_mult);
    fprintf('Inverse: %f\n', t_inv);
    fprintf('Pseudo-inverse: %f\n\n', t_pinv);
end
